# Challenge

El challenge consta de implementar, con Terraform, un ambiente de una aplicacion con al menos los siguientes servicios montados en contenedores de Docker:

	-	Frontend: 	Se recomienda, pero no es obligatorio, utilizar Angular.
	-	Backend: 	Se recomienda, pero no es obligatorio, utilizar Java (Spring Boot).
	-	Proxy: 		Implementar un proxy reverso.
	-	Database: 	Se recomienda, pero no es obligatorio, utilizar PostgreSQL o MongoDB.
	-	Logging		

*Requerimientos*:
	- Utilizar docker.
	- Utilizar Terraform
	- Documentación de cómo se implementó, si hay credenciales y de cómo ejecutarlo.

*Aclaraciones*:
	-  No es necesario que desarrolle los servicios, puede tomar algún template y aplicarlo al challenge.

Bonus, son opcionales pero se valoran mucho:

- 	Implementar Kubernetes.
- 	Documentacion de las mejoras implementadas, si es que las hay.
-   Configurar el ambiente como un modulo de Terraform.
- 	Hacer tests que verifiquen el funcionamiento.





## Solucion:

Para este challenge hice pruebas en diferentes contextos (Docker, Kubernetes, Local) y tecnologias (Angular, React, MongoDB, PostgreSQL, NGiNX, springboot, Envoy, Consul, prometheus, Graphana) considerando lo requerido (Frontend & Backend), en cada una de las carpetas del repositorio podran ver los scripts o recetas utilizados durante el challenge, las apps demos utilizadas fueron tomadas de github.com


Cuando se trata de construir Microservicios o se esta en proceso de transformacion a Microservicios se deben tomar en cuenta muchos aspectos, una de las principales razones de los Microservicios es garantizar Agilidad de construccion, Despliegue, Mantenimiento y Recuperacion, Alta Disponibilidad, Seguridad en cada punto de nuestros servicios, trazabilidad al momento de una auditoria y Observabilidad.

Llevando este challenge a las buenas practicas y a un nivel confiable se utilizaron tecnologias maduras y que pudieran aportar todas los aspectos antes mencionados.



![](/Images/Diagram.png)





A continuacion mencionare posibles soluciones que aplique a este challenge:


## Build, integration & Deploy:

Para este punto Jenkins nos da la capacidad de integrar, construir y probar nuestra aplicacion utilizando las distintas librerias y plugins que nos proporciona, una vez construida la imagen de nuestra aplicacion jenkins la subiria a nuestro depositorio de imagenes, en este paso podriamos utilizar Gitlab y Terraform Cloud como herramienta de automatizacion de despliegue de nuestra aplicacion e infraestructura a distintas Clouds privadas o publicas y a diferentes entornos utilizando nuestras recetas terraform.



## Alta Disponibilidad:

Para Garantizar el HA y recovery de nuestros servicios considere utilizar Kubernetes (MiniKube 1 Master) ya que podemos configurar tantas replicas como necesitemos de nuestros servicios o apps. 

Tambien decidi deplegar dentro de Kubernetes Hashicorp Consul como service Discovery y Health Checker integrado con Envoy para cada POD para el routing de los servicios y popular la registry de Consul, hacer split de carga y tambien como ingress-gateway, tambien como feature Envoy agrega la capa 7  para observabilidad, obtener data y salud de los servicios. 

![](/Images/Screenshot_from_2020-10-13_12-18-08.png)







![](/Images/Screenshot_from_2020-10-13_12-24-08.png)




## Monitoring y Logging:

Desplegue un servidor de Prometheus para colectar logs de los servicios y Graphana para Monitoring. Contiene un Dashboard creado para ver la salud de los servicios y del cluster, tambien monitorea la carga de cada uno de los servicios.

![](/Images/Screenshot_from_2020-10-13_12-22-34.png)


_____________________________________________________________________________________________________________________________________________


![](/Images/Screenshot_from_2020-10-13_12-29-05.png)



## Seguridad:

Ademas de la capa de seguridad que agrega Envoy y Consul (L7 y L4) a nuestros servicios, debemos agregar una capa mas de seguridad para el consumo de la Aplicacion desde afuera, para esto propongo utilizar OauthProxy e integrarlo con Keycloack. con estas herramientas podemos añadir politicas de acceso, politicas de consumo, ser mas especifico con respecto a las personas al momento de entregar nuestros servicios y mayor trazabilidad de los eventos de nuestras aplicaciones. 

![](/Images/1_h_kboYu0siZx2cdcOg_3xg.png)


Para el consumo y administracion de secretos a nivel de certificados y credenciales entre servicios  podriamos agregar un servidor de Hashicorp Vault de esta manera cada secreto estaria cifrado. Por Ejemplo: la conexion de nuestra aplicacion necesita credenciales para conectarse a la base de datos, para esto configurariamos los secretos de nuestra BD en Vault y nuestra aplicacion los consumiria de manera cifrada agregando mas seguridad y buenas practicas a nuestros servicios.







"Como buena practica recomiendo implementar 2 cluster de kubernetes en modo activo-activo con un esquema (3 master, 3 workers, 3 infra) c/u y distribuir nuestros servicios entre ellos"

Es importante saber que podemos desplegar cluster de kubernetes en Clouds Publicas y privadas.









