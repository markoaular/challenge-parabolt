1.Instalar node,angular cli

2. crear angular project
$ ng new YOUR_APP_NAME
$ cd YOUR_APP_NAME


Crear Dockerfile

FROM node:slim as build
RUN mkdir /home/app && chown node:node /home/app
WORKDIR /home/app
RUN npm install -g @angular/cli
USER node
COPY package.json package-lock.json* ./
RUN npm install --no-optional && npm cache clean --force
ENV PATH /home/app/node_modules/.bin:$PATH
COPY --chown=node:node . .
CMD ng serve --host 0.0.0.0
