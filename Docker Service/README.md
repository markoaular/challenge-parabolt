Proyecto fullstack con angular, spring-boot, mongodb y docker. Primero, crearemos un proyecto angular simple que tiene un botón. Si hacemos clic en el botón, enviará una solicitud al backend (proyecto spring-boot) para agregar un nuevo elemento y recuperar todos los elementos. Después de obtener los datos, mostrará los datos obtenidos en una lista en la frontend. Crearemos un archivo docker y acoplaremos la frontend y enviaremos la imagen del frontend acoplada a nuestra cuenta de dockerhub.

A continuación, crearemos un proyecto de arranque de primavera simple que tendrá 2 API (getItems y addItem) para agregar un nuevo elemento y obtener elementos Todos los datos se guardarán en una base de datos mongodb.

Hay dos escenarios prácticos para ejecutar un

- Ejecute la aplicación spring-boot localmente; ejecutar mongodb localmente
- Ejecute la aplicación spring-boot localmente; ejecutar mongodb en docker
- Ejecute la aplicación spring-boot en docker; ejecutar mongodb en docker

[1] Si ejecutamos ambos localmente, no tenemos que preocuparnos por el puerto y el host de mongodb. Simplemente instale y ejecute mongo localmente y ejecute la aplicación Spring. No olvide agregar mongodb en pom.xml. Spring se conectará al mongodb a través del puerto predeterminado de 27017 y también, si desea cambiar el puerto, puede configurarlo en el archivo application.properties o usando la línea de comando durante la ejecución de la aplicación spring-boot.

[2] Si queremos ejecutar mongodb en docke y queremos ejecutar la aplicación spring-boot localmente, entonces necesitamos ejecutar la imagen de mongo exponiendo el contenedor a través de un puerto al localhost. En este caso, el contenedor mongo puede hablar / comunicarse con el host local desde el contenedor. Por ejemplo:

docker run -p 27027: 27017 --nombre mongodb_container mongo

Obtendrá la imagen de mongo y ejecutará un contenedor en el puerto 27017 de docker. Luego, expondrá el contenedor en el localhost a través del puerto 27027. Entonces, para conectarnos al mongoDB en tal caso, tenemos que definir el puerto mongo específico (que expusimos desde el contenedor al localhost) en el archivo application.properties como spring.data.mongo.port = 27027 Ahora, si ejecutamos el Spring-boot, entonces puede conectarse a mongoDB.

[3] Si queremos ejecutar mongodb en docker y también queremos ejecutar la aplicación spring-boot en docker, primero ejecute la imagen de docker mongo

docker run -p 27027: 27017 --nombre mongodb_container mongo

Luego use el nombre del contenedor mongo como el host mongo. Por ejemplo, en application.properties use spring.data.mongo.host = mongodb_container; spring.data.mongo.port = 27017 También deben estar en la misma red para comunicarse entre sí. Podemos hacer esto durante la ejecución de la imagen de docker o podemos usar el archivo docker-compose. En este caso, es fácil y una buena práctica utilizar un archivo docker-compose.