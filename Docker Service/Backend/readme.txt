
Instrucciones
1. Crear spring project
2. Agregar todos los archivos y cambios requeridos
3. Build spring project

$ mvn clean package && java -jar target/app.jar


4. Drear Dockerfile
FROM openjdk:8
VOLUME /temp
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
EXPOSE 8080
ENTRYPOINT  java -jar app.jar

$ docker build .

