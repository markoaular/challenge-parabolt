# Para reproducir utilizando Terraform


Pre-requsitos:
- kubernetes EKS utilizando el modulo Kubernetes para aws, tambien puede reproducirlo sobre minikube o microk8s modificando el contexto en las recetas terraform.
- Terraform CLI
- Configurar aws profile opcional, tambine puede utilizar el archivor provider.tf para configurar el acceso a aws

Ejecutar:

$ Terraform init


Despues de inicializado el workspace

Ejecutar:

$ terraform plan

Luego ejecutar:

$Terraform apply
