resource "kubernetes_deployment" "backend" {
  metadata {
    name = "contact-backend-deployment"
    labels = {
      App = "contacts-backend"
    }
  }

  spec {
    replicas = 2
    selector {
      match_labels = {
        App = "contacts-backend"
      }
    }
    template {
      metadata {
        labels = {
          App = "contacts-backend"
        }
      }
      spec {
        container {
          image = "raghav141988/contacts-backend"
          name  = "contacts-backend"

          port {
            container_port = 8080
          }

          env {

            name = "MONGODB_HOST"
            value = "contacts-mango-cluster-ip-service"

            name = "MONGO_INITDB_ROOT_USERNAME"
            value = "root"

            name = "MONGO_INITDB_ROOT_PASSWORD"
            value = "MANGOPASSWORD"
            }


          }

          resources {
            limits {
              cpu    = "0.5"
              memory = "512Mi"
            }
            requests {
              cpu    = "250m"
              memory = "50Mi"
            }
          }
        }
      }
    }
  }
}