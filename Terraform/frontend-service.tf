resource "kubernetes_service" "contact-frontend" {
  metadata {
    name = "contact-frontend-server-cluster-ip"
  }
  spec {
    selector = {
      App = contacts-frontend
    }
    port {
      port        = 4200
      target_port = 4200
    }

    type = "ClusterIP"
  }
}