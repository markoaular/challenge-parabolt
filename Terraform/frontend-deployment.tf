resource "kubernetes_deployment" "frontend" {
  metadata {
    name = "contact-frontend-deployment"
    labels = {
      App = "contacts-frontend"
    }
  }

  spec {
    replicas = 2
    selector {
      match_labels = {
        App = "contacts-frontend"
      }
    }
    template {
      metadata {
        labels = {
          App = "contacts-frontend"
        }
      }
      spec {
        container {
          image = "raghav141988/contacts-frontend"
          name  = "contacts-frontend"

          port {
            container_port = 4200
          }

          resources {
            limits {
              cpu    = "0.5"
              memory = "512Mi"
            }
            requests {
              cpu    = "250m"
              memory = "50Mi"
            }
          }
        }
      }
    }
  }
}