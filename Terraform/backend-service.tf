resource "kubernetes_service" "contact-backend" {
  metadata {
    name = "contact-backend-server-cluster-ip"
  }
  spec {
    selector = {
      App = contacts-backend
    }
    port {
      port        = 8080
      target_port = 8080
    }

    type = "ClusterIP"
  }
}