resource "kubernetes" "api-backend" {
  metadata {
    name = "contact-backend-api"
    labels = {
      App = "contact-backend-api"
    }
  }

  spec {
    replicas = 2
    selector {
      match_labels = {
        App = "contact-backend-api"
      }
    }
    template {
      metadata {
        labels = {
          App = "contact-backend-api"
        }
      }
      spec {
        container {
          image = "raghav141988/contact-backend-api"
          name  = "contact-backend-api"

          port {
            container_port = 8081
          }

          resources {
            limits {
              cpu    = "0.5"
              memory = "512Mi"
            }
            requests {
              cpu    = "250m"
              memory = "50Mi"
            }
          }
        }
      }