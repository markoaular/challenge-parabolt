resource "kubernetes_deployment" "mongo" {
  metadata {
    name = "mongo-deployment"
    labels = {
      App = "mongo-deployment"
    }
  }

  spec {
    replicas = 2
    selector {
      match_labels = {
        App = "mongo-database"
      }
    }
    template {
      metadata {
        labels = {
          App = "mongo-database"
        }
      }
      spec {
        container {
          image = "mongo:4.0.0"
          name  = "mongo"
          command = ["/bin/bash", "-c", "mv /usr/bin/numactl /usr/bin/numactl1 && source docker-entrypoint.sh mongod"]

          port {
            container_port = 27017
          }

         volume {
             name = "mongo-storage"
         }

         volume_mount {
            name       = "mongo-storage"
            mount_path = "/data/db"
          }

