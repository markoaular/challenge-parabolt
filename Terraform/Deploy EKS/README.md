## Pre-requisitos:

- Configurar el perfil aws 


`$ aws configure`

`AWS Access Key ID [None]: <YOUR_AWS_ACCESS_KEY_ID>`

`AWS Secret Access Key [None]: <YOUR_AWS_SECRET_ACCESS_KEY>`

`Default region name [None]: <YOUR_AWS_REGION>`

`Default output format [None]: json`

Esto permite que Terraform acceda al archivo de configuración y realiza operaciones en su nombre con estas credenciales de seguridad.

Una vez hecho esto, inicialice su espacio de trabajo de Terraform, que descargará el proveedor y lo inicializará con los valores proporcionados en el archivo terraform.tfvars.

`$ terraform init

Initializing modules...

Downloading terraform-aws-modules/eks/aws 9.0.0 for eks...
- eks in .terraform/modules/eks/terraform-aws-modules-terraform-aws-eks-908c656
- eks.node_groups in .terraform/modules/eks/terraform-aws-modules-terraform-aws-eks-908c656/modules/node_groups
Downloading terraform-aws-modules/vpc/aws 2.6.0 for vpc...
- vpc in .terraform/modules/vpc/terraform-aws-modules-terraform-aws-vpc-4b28d3d

`Initializing the backend...

Initializing provider plugins...
- Checking for available provider plugins...
- Downloading plugin for provider "template" (hashicorp/template) 2.1.2...
- Downloading plugin for provider "kubernetes" (hashicorp/kubernetes) 1.10.0...
- Downloading plugin for provider "aws" (hashicorp/aws) 2.52.0...
- Downloading plugin for provider "random" (hashicorp/random) 2.2.1...
- Downloading plugin for provider "local" (hashicorp/local) 1.4.0...
- Downloading plugin for provider "null" (hashicorp/null) 2.1.2...

Terraform has been successfully initialized!`



Una vez inicializado el espacio de trabajo

Ejecute:

$terraform plan

Con esto visualiza los recursos y configuraciones que seran creados.

Despues ejecute:

$ terraform apply




## Implementar el servidor de métricas de Kubernetes

El servidor de métricas de Kubernetes, que se utiliza para obtener métricas como la CPU del clúster y el uso de la memoria a lo largo del tiempo, no se implementa de forma predeterminada en los clústeres de EKS.

Descargue y descomprima el servidor de métricas ejecutando el siguiente comando:

`$ wget -O v0.3.6.tar.gz https://codeload.github.com/kubernetes-sigs/metrics-server/tar.gz/v0.3.6 && tar -xzf v0.3.6.tar.gz`



Despues ejecute:

`$ kubectl apply -f metrics-server-0.3.6/deploy/1.8+/`

## Implementar el dashboard de Kubernetes

El siguiente comando programará los recursos necesarios para el Dashboard:

`kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0-beta8/aio/deploy/recommended.yaml`



Ahora, cree un servidor proxy que le permitirá navegar al dashboard desde el navegador en su máquina local. Esto continuará ejecutándose hasta que detenga el proceso presionando CTRL + C.


`$ kubectl proxy`

para acceder al dashboard

`http://127.0.0.1:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/`




Autenticarse en el Dashboard

Para utilizar el dashboard de Kubernetes, debe proporcionar un token de autorización. La autenticación mediante kubeconfig no es una opción. Puede leer más sobre esto en la documentación de Kubernetes.

Genere el token en otra terminal (no cierre el proceso de proxy de kubectl).


```shell
$ kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep service-controller-token | awk '{print $1}')

Name:         service-controller-token-46qlm
Namespace:    kube-system
Labels:       <none>
Annotations:  kubernetes.io/service-account.name: service-controller
              kubernetes.io/service-account.uid: dd1948f3-6234-11ea-bb3f-0a063115cf22

Type:  kubernetes.io/service-account-token

Data
====
ca.crt:     1025 bytes
namespace:  11 bytes
token:      eyJhbGciOiJSUzI1NiIsImtpZCI6I...
```


copie el token y peguelo en el dashboard para hacer login.
