resource "kubernetes_service" "mongo-service" {
  metadata {
    name = "contacts-mongo-cluster-ip-service"
  }
  spec {
    selector = {
      App = contacts-mongo-cluster-ip-service
    }
    port {
      port        = 27017
      target_port = 27017
    }

    type = "ClusterIP"
  }
}